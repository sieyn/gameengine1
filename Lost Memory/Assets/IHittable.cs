﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IHittable
{
    // 있어야 한다는 것만 알려주는 것.
    float HitPoint { get; }

    void TakeDamage(float damageAmount);



}