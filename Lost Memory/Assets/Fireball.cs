﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    private Rigidbody myRigidbody;

    public int Damage = 20;
    public float Speed = 100.0f;

    [SerializeField] private Transform targetTransform;

    private void Awake()
    {
        transform.Translate(Vector3.up, Space.World);
        transform.Rotate(new Vector3(1,0,0), 90);
        transform.Rotate(new Vector3(0, 1, 0), 90);
        transform.Rotate(new Vector3(-1, 0, 0), 90);
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (targetTransform != null)
        {

            GetComponent<Rigidbody>().AddForce(-transform.up * (Time.deltaTime * Speed));
            //transform.Translate(-Vector3.up * (Time.deltaTime * Speed), Space.World);
            if (transform.position.x + 5 >= targetTransform.position.x)
            {
                if (transform.position.z + 5 >= targetTransform.position.z && transform.position.z - 5 <= targetTransform.position.z)
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                Destroy(gameObject, 3);
            }
        }
    }

}
