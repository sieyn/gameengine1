﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public enum GameState
	{
		Start,
		End
	}
	public static GameManager manager;

	public static GameManager Instance
	{
		get
		{
			if (manager == null)
				manager = GameObject.FindObjectOfType<GameManager>();
			if (manager == null)
			{
				var gameObject = new GameObject(name: "Game Manager");
				manager = gameObject.AddComponent<GameManager>();
			}
			return manager;

		}

	}

	private GameState state;
	public event Action<GameState> gameStateChanged;

	[SerializeField]
	private Player player;


	private void Awake()
	{
		manager = this; // 나 자신을 넣음.


	}
// Start is called before the first frame update
	void Start()
    {
		StartCoroutine(Fsm());
	}

	IEnumerator Fsm()
	{
		while (true)
		{
			yield return null;
		}
	}

	public void StateChange(GameState after)// 외부에서 state를 수정하기 위해서.
	{
		gameStateChanged?.Invoke(after); // 이벤트를 실행시킴	
										 // ? : 이 값이 널이라면 이 뒤를 실행시키지 않도록. 
		state = after;
	}

	// Update is called once per frame
	void Update()
    {


    }
}
