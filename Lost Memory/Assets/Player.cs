﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IHittable
{
    [SerializeField]
    public float maxHp = 200;
    public float hp;

    public float hitPoint;
    public float HitPoint { get; }

    [SerializeField] private float speed = 10f;
    Rigidbody _rigidbody;

    Vector3 movement;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    
    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Vertical");
        float v = Input.GetAxisRaw("Horizontal");
        Run(h, v);
    }

    void Run(float h, float v)
    {
        movement.Set(h, 0, -v);
        movement = movement.normalized * speed * Time.deltaTime;

        _rigidbody.MovePosition(transform.position + movement);
    }

    // Start is called before the first frame update
    void Start()
    {
        hp = maxHp;
       
    }

    // Update is called once per frame
    void Update() // 프레임에 따라 1초에 시행되는 수가 다르다.
    {
        //if (hp > 0)
        //{
        //    // 60 프레임이라면 deltatime이 1/60이 될 것.
        //    hp = hp - Time.deltaTime; // 1초에 1번 깎기.
        //}

        if (hp < 0)
        {
            hp = 0;
        }

        GetInput();
    }

    private void GetInput()
    {


        if (Input.GetKeyDown(KeyCode.T))
        {
            GameManager.Instance.StateChange(GameManager.GameState.Start);
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            GameManager.Instance.StateChange(GameManager.GameState.End);
        }

    }

    public void TakeDamage(float damageAmount)
    {
        hitPoint = hitPoint - damageAmount;
    }
}
