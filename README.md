# GameEngine1

오시은 2016180029 게임공학과

1. 게임 개요
게임 제목 : Lost memory
숲에서 기억을 잃고 깨어난 주인공이 숲 속 몬스터를 해치우며 기억에 대한 단서를 찾아다니는 스토리.
자연을 바탕으로 한 판타지 테마. 몬스터 사냥을 통해 경험치를 획득하는 1인 성장형 RPG 게임.

2. 게임 조작법
이동키 : 화살효 위, 아래, 왼쪽, 오른쪽
공격 키 : backspace 키 및 플레이어 주변의 도트데미지.

3. 개발에 사용한 외부 라이브러리 리스트 및 소개

Nature Starter Kit 2 - 전체 배경
Skybox Series Free - skybox
Small Red Dragon - 주인공 모델링
Fantasy Rhino - 몬스터 모델링
Procedural fire - 불꽃 이펙트 효과

4. 강의를 통해 학습한 부분 활용
핵심 기설 설명 : 
가장 중요하게 사용한 transform.translate, Vector3.MoveTowards, Vector3.Lerp 를 통한 몬스터와 캐릭터 오브젝트 이동과 회전.
'파티클 이펙트'를 사용하여 맵 주변 몽환적인 분위기 조성. 
노을이 지는 듯한 아름다운 배경.

강의에서 진행한 부분 중 사용된 부분 : 
 Box Collider를 사용하여 다른 오브젝트와의 충돌처리를 체크.
[SerializeField]를 사용하여 private 멤버들 유니티에서 접근.
수학함수 Mathf.lerp를 사용한 보간값을 통해 몬스터 이동.
싱글톤을 이용한 GameManager 구조와 무한 상태 머신 fsm.
상속을 이용하여 카메라가 캐릭터를 따라다니게끔 구성.
GetComponent<>()를 통해 오브젝트의 transform position을 가져와서 몬스터가 플레이어를 추적하거나 공격 이펙트가 몬스터에
닿았는지 확인하는 데 사용.




