﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour, IHittable
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private float speed = 3;
    [SerializeField] private float viewAngle;

    public float someValue = 0;
    public float destValue = 500;

    public float hitPoint;
    public float HitPoint { get; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerTransform != null)
        {

            //var angle = Vector3.Dot(transform.forward, playerTransform.forward);

            var distance = Vector3.Distance(transform.position, playerTransform.position);
            //if (distance <= 3 && Mathf.Cos(viewAngle * Mathf.Deg2Rad) < angle)
            //{
            //    transform.position = Vector3.MoveTowards(transform.position, playerTransform.position, Time.deltaTime * speed);
            //    transform.RotateAround(playerTransform.position, Vector3.right, Time.deltaTime * speed);
            //}

            if (distance <= 17)
            {
                Destroy(gameObject, 2);
                Destroy(this, 2);
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, playerTransform.position, Time.deltaTime * speed);
            }

            //someValue = Mathf.Lerp(someValue, destValue, Time.deltaTime);
            //transform.position = new Vector3(someValue, transform.position.y, transform.position.z);
        }
    }
    public void TakeDamage(float damageAmount)
    {
        hitPoint = hitPoint - damageAmount;
    }

    void OnCollisionEnter(Collision Col)
    {

    }
}
