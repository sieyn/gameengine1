﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public GameObject fireball;
    public Transform firePos;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            if (fireball != null)
            {
                GoFire();
            }
            else
            {
                //fireball = new GameObject();
            }
        }
    }

    void GoFire()
    {
        CreateFireball();
    }

    void CreateFireball()
    {
        Instantiate(fireball, firePos.position, firePos.rotation);
    }
}
